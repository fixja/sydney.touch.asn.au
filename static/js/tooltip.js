jQuery(document).ready(function($) {
	var truncatedCSS = {
		position : 'relative'
	}
	var tooltipCSS = {
		backgroundColor : '#fdfad0',
		position : 'absolute',
		opacity : 0,
		top : '0px',
		left : '0px',
		padding : '10px',
		border : '1px solid #666',
		zIndex : '1000'
	}
	$('.truncated').css(truncatedCSS);
	$('body').append('<div id="tooltip"></div>');
	$('#tooltip').css(tooltipCSS);
	$('#tooltip').hide(); // visibility : hidden
	$('.truncated').mouseover(function() {
		var offset = $(this).offset();
		$('#tooltip').html($(this).attr('title')).css({
			top : offset.top + $(this).height() + 'px',
			left : offset.left + 'px'
		}).show().animate({
			opacity : 0.8 // fade it in
		}, 400, 'swing');
	});
	$('.truncated').mouseout(function() {
		$('#tooltip').stop().hide().css(tooltipCSS).html('');
	});
});