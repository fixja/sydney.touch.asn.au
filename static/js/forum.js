var xmlHttp

function update_post(element_id, post_id) {
	xmlHttp = GetXmlHttpObject ()
	
	if (xmlHttp == null) {
		alert ("Your browser does not support Ajax.");
		return;
	}

	var updated = 'post_'+post_id+'_updated';
	var url = '/forum/update/' + post_id + '/';
	var parameters = "data=" + encodeURIComponent( document.getElementById(element_id).innerHTML );
	var ed = tinyMCE.get(element_id);

	ed.setProgressState(1);
	/* alert(parameters); */

	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4) {
			ed.setProgressState(0);
			toggleEditor(element_id);
			document.getElementById (element_id).innerHTML = xmlHttp.responseText;
			document.getElementById (updated).innerHTML = '<strong>Successfully Updated</strong>';
		}
	};
	xmlHttp.open ("POST", url, true);
	xmlHttp.send (parameters);
}

function GetXmlHttpObject () {
	var xmlHttp = null;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest ();
	}
	catch (e) {
		// Internet Explorer
		try {
			xmlHttp = new ActiveXObject ("Msxml2.XMLHTTP");
		}
		catch (e) {
			xmlHttp = new ActiveXObject ("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
