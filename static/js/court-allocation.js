$(document).ready( function() {

	function dropAllocation(ev, ui) {
		var obj = ui.draggable.length ? ui.draggable[0] : null;
		var container = $(obj).parent();

		/* re-enable the container if it was a droppable */
		if (container.hasClass('ui-droppable-disabled')) {
			container.droppable('enable');
		}

		/* set the top/left coordinates so we don't have problems with 'revert' */
		obj.style.top = '';
		obj.style.left = '';

		/* move this element around in the DOM and disabled it from receiving any new elements */
		$(this).append(obj);
		if (!$(this).parent().hasClass('matchlist')) {
			$(this).droppable('disable');
		}

		/*  */
		$('.ui-droppable.active').removeClass('active');
	}

	$(".match").draggable({ opacity: 0.5, revert: true });
	$(".court div").droppable({ accept: ".match", activeClass: "active", hoverClass: "over", tolerance: "pointer", drop: dropAllocation });
	$(".matchlist div").droppable({ accept: ".court .match", activeClass: "active", hoverClass: "over", tolerance: "pointer", drop: dropAllocation });

	/* re-root the matches that have actually already been allocated */
	function resetAllocations() {
		$('.match').each( function() {
			var ids = this.id.match(/\d+/g);
			if (ids.length > 1) {
				var alloc = $('.timeslot[name=' + ids[1] + '] .court div[id$=_' + ids[2] + ']')
				if (alloc.length) {
					alloc.append(this);
					alloc.droppable('disable');
				}
			}
		});
	}
	resetAllocations();

	function updateForm() {
		$('.court .match').each( function() {
			/* from the dom structure, get the values we need for our form */
			var match = this.id.match(/\d+/g)[0];
			var court = this.parentNode.id.match(/\d+$/)[0];
			var time = this.parentNode.parentNode.parentNode.getAttribute('name');

			/* get the form elements for convenience - easier to set the values */
			var f1 = $('input#id_m'+match+'-court');
			var f2 = $('input#id_m'+match+'-datetime');

			f1[0].value = court;
			f2[0].value = f2[0].value.replace(/\d+:\d+:\d+/, time.substring(0,2) + ':' + time.substring(2,4) + ':00');
		});
	}

	$('form').each ( function() {
		$(this).bind('submit', updateForm);
	});

});
