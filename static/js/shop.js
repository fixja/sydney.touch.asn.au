$(document).ready(function() {
	$('.photos .secondary').mouseover(function() {
		$('#photo').css('background-image', 'url('+this.children[0].src+')');
	});
	$('.photos .secondary').mouseout(function() {
		var node = $('#photo');
		node.css('background-image', 'url('+node[0].children[0].src+')');
	});
});
