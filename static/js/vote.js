var xmlHttp

function cast_vote(poll, choice) {
	xmlHttp = GetXmlHttpObject ()
	
	if (xmlHttp == null) {
		alert ("Your browser does not support Ajax.");
		return;
	}

	var c = 0;
	for (i=0; i<choice.length; i++) {
		if (choice[i].checked)
			c = choice[i].value;
	}

	var url = "/polls/" + poll + "/vote/" + c + "/";

	xmlHttp.onreadystatechange = stateChangedVote;
	xmlHttp.open ("GET", url, true);
	xmlHttp.send (null);
}

function stateChangedVote() {
	if (xmlHttp.readyState == 4) {
		document.getElementById ("vote").innerHTML = xmlHttp.responseText;
	}
}

function GetXmlHttpObject () {
	var xmlHttp = null;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest ();
	}
	catch (e) {
		// Internet Explorer
		try {
			xmlHttp = new ActiveXObject ("Msxml2.XMLHTTP");
		}
		catch (e) {
			xmlHttp = new ActiveXObject ("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
